# Top-k Temporal Closeness for Temporal Networks

Data sets and appendix of our [IEEE International Conference on Data Mining (ICDM 2020)](http://icdm2020.bigke.org/) paper *"Efficient Top-k Temporal Closeness Calculation in Temporal Networks"*.


## Data sets

The data sets are in the folder datasets. 
Unzip the file `datasets.zip`. 

## Source Code

The source code is available as part of the temporal graph library [TGLib](https://gitlab.com/tgpublic/tglib).


## Contact 
If you have any questions, send an email to Lutz Oettershagen (lutz.oettershagen at cs.uni-bonn.de).

